\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{subfigure}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{textcomp}
\usepackage{multirow}
\usepackage{xcolor}
\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\renewcommand{\algorithmicrequire}{ \textbf{Input:}}
\renewcommand{\algorithmicensure}{ \textbf{Output:}}

\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
\begin{document}

\title{The Communication Performance of BCDC Data Center Network\\
{\footnotesize \textsuperscript{}}
}

\author{\IEEEauthorblockN{Shuangxiang Kan, Jianxi Fan$^{*}$, Baolei Cheng}
\IEEEauthorblockA{School of Computer Science and Technology,\\ Soochow University \\
Suzhou, China \\
jxfan@suda.edu.cn}
\and
\IEEEauthorblockN{Xi Wang}
\IEEEauthorblockA{School of Software and Services Outsourcing,\\ Suzhou Institute of Industrial Technology \\
Suzhou, China \\
wangxi0414@suda.edu.cn}
}

\maketitle

\begin{abstract}
The BCDC network is a new server-centric data center network based on crossed cube. Its decentralized and recursively defined features solve the bandwidth bottleneck problem of the upper switch in the tree structure and make it more scalable. In addition, the degree of BCDC server is constant, which reduces connection cost and technical difficulty relative to DCell and BCube. In this paper, we study and analyze the data communication, fault tolerance, and node-disjoint paths of BCDC through practical experiments. The results show that BCDC is superior to DCell and Fat-Tree in data communication. Moreover, in terms of fault-tolerant routing and node-disjoint paths, the performance of BCDC is no worse than that of DCell and Fat-Tree, which provides an important basis for the design and application of the new data center network.\\
\end{abstract}

\begin{IEEEkeywords}
BCDC, data communication, fault tolerance, node-disjoint paths, data center network
\end{IEEEkeywords}

\section{Introduction}

The performance of the data center network largely determines the performance of cloud computing. Therefore, we can improve the performance of cloud computing by building a data center network with good performance. As the size and complexity of data center networks grows, so does the number of servers in data center networks. For example, Google had 450,000 servers in 2006, and by the end of 2010, its number of servers had reached more than 900,000. In a data center network with such a large number of servers, how to connect these servers to form a good performance data center network is a challenge to improve cloud computing performance. There are many factors that determine the performance of a data center network, communication performance and fault tolerance are significant aspects of them:\\
\textbf{Communication performance}: Many applications deployed on data center networks, such as web page retrieval, games, etc., have much more traffic between servers than traffic with external customers. These applications are all implemented through data communication between servers. Therefore, a good data center network should be able to support some typical data transmission methods, such as one-to-one, one-to-many, one-to-all, and all-to-all, etc.
On the other hand, improving data transmission bandwidth is also the goal of data center network design. \\
\textbf{Fault tolerance}: Due to the large number of switches and servers in the data center network, it is difficult to avoid the failure of switches,  servers, and links. Therefore, in the application of the data center network, it must be considered how to make the network work normally when some resources (servers, switches, or links) fail, that is, the data center network must consider fault tolerance.

\textbf{Scalability} and \textbf{cost} are also two important aspects of evaluating data center network. With the continuous expansion of data scale, good scalability and low cost will effectively promote the expansion of data center network and the processing of data.

In order to meet the above requirements, various data center networks have been proposed so far\cite{AlFaresM2008,GuoC2008,GuoC2009,Zhengk2016,BhuyanLN1984,XuM2019,NasirianS2019,XieP2019}. Such as traditional tree structure, Fat-Tree \cite{AlFaresM2008,LeisersonCE1985,MisraS2019}, DCell\cite{GuoC2008,LvM2018}, and BCube\cite{GuoC2009}, etc. However, these factors are mutually influential and mutually restrictive. For example, although the Fat-Tree is simple in structure and easy to implement, its tree structure makes it weak in fault tolerance, uneven bandwidth distribution, insufficient scalability, and high cost. It cannot support one-to-many and many-to-many network communication well; The data center network represented by DCell has better routing performance and high scalability. However, when the switches become faulty, the length of the fault-tolerant path between servers will increase significantly.

A new data center network based on crossed cube was proposed in \cite{wang2018bcdc}, which is denoted by BCDC. An $n$-dimensional BCDC can be constructed recursively from two $(n-1)$-dimensional BCDCs. In an $n$-dimensional BCDC, each server has 2 ports connected to 2 switches, and each switch is equipped with $n$ ports connected to $n$ servers. Because each server is equipped with two ports, the reliability of BCDC will not be greatly affected even if one of the ports becomes faulty.

A lot of theoretical work has been done on BCDC network, so in this paper, we will focus on the experimental research of BCDC properties. This will provide an important basis for the deployment possibility of BCDC. We carry out experiments on data communication, fault tolerance and node-disjoint paths of $3$-dimensional BCDC. At the same time, we also conducted corresponding comparison experiments on two other common data center networks, DCell and Fat-Tree. The experimental results show that BCDC has excellent data communication performance, and its performance in fault tolerance and node-disjoint paths is not inferior to DCell and Fat-Tree.

The organization of the paper is as follows, we will give the definition and construction method of BCDC in section \ref{structure}. Then in section \ref{experiment}, experiments and analysis of the BCDC data communication, fault-tolerant routing, and node-disjoint paths are given. Section \ref{conclusion} concludes the paper.


\section{Physical and Logical Structure of BCDC}\label{structure}

We use the graph $G$ to represent a data center network. The switch can be considered as a transparent network device\cite{GuoC2008} in the data center network, so the node can be used to represent the server and the edge represents the link between servers in the data center network. We use $V(G)$ and $E(G)$ to represent the node set and edge set of the graph $G$, respectively.



As mentioned above, we intend to construct a data center network based on the $n$-dimensional crossed cube, denoted by $CQ_n$, which has better performance in many aspects than other networks. See \cite{EfeK1992,DBLPEfe,Chang2000Edge,GuoJ2019,WangS2018,ChenHC20} for the specific definition of crossed cube.

BCDC is a data center network based on the crossed cube. We will deploy switches on the nodes of $CQ_n$ and servers on the edges of $CQ_n$. For the convenience of description, We do not distinguish between each switch and its address. The same is true for each server and its address.

The address of each switch is denoted by a $n$-bit binary string $x=x_{n-1}x_{n-2}\ldots x_0$. The address of each server is represented as an ordered pair $[x,y]$, where $x$ and $y$ are two $n$-bit binary strings $x=x_{n-1}x_{n-2}\ldots x_0$ and $y=y_{n-1}y_{n-2}\ldots y_0$. A server $[x,y]$ is connected to a switch $u$ if and only if $u\in \{x,y\}$ and $(x,y)\in E(CQ_n)$. Thus, we derive the original graph $A_n$ of the $n$-dimensional BCDC network.

Considering that the switch is transparent in the BCDC network, we can give the definition of the logical graph of the BCDC network as follows:

\begin{figure*}[htbp]
\centerline{\includegraphics[scale=0.25]{A3B3.eps}}
\caption{(a)The original graph of 3-dimensional BCDC $A_3$,
(b)The logical graph of 3-dimensional BCDC $B_3$.}
\label{original-logical-bcdc3}
\end{figure*}

%\begin{figure*}[ht]
%\centering
%% Requires \usepackage{graphicx}
%\includegraphics[scale=0.25]{A3B3.eps}\\
%\caption{(a)The original graph of 3-dimensional BCDC $A_3$,
%(b)The logical graph of 3-dimensional BCDC $B_3$}
%\label{original-logical-bcdc3}
%\end{figure*}


\begin{definition}
\label{Defi-BCDCn}
\cite{wang2018bcdc}
The $n$-dimensional BCDC network$,$
$B_n,$
is recursively defined as follows$.$
$B_2$ is a cycle with $\rm{4}$ nodes
$[00,01],$
$[00,10],$
$[01,11],$ and
$[10,11].$
For $n \geq 3,$
we use $B^0_{n-1}$ $($resp$.$ $B^1_{n-1}$$)$ to denote
the graph obtained by $B_{n-1}$ with changing each node $[x,y]$ of $B_{n-1}$ to $[0x,0y]$ $($resp$.$ $[1x,1y]$$).$
$B_n$ consists of $B^0_{n-1}$, $B^1_{n-1},$  and a node set
$S_n = \{ [a,b] | a \in V(CQ_{n - 1}^0),$ $b \in V(CQ_{n - 1}^1),$ and $(a,b) \in E(C{Q_n})\}$
according to the following rules$.$
For nodes  $u = [a,b] \in V(B_{n - 1}^0),$ $v = [c,d] \in S_n,$ and $w = [e,f] \in V(B_{n - 1}^1)$$:$

$1)$  $(u,v) \in E(B_n)$ if and only if $a = c$ or $b = c.$

$2)$  $(v,w) \in E(B_n)$ if and only if $e = d$ or $f = d.$
\end{definition}


The original and logical graphs of the $3$-dimensional BCDC network are shown in Fig. \ref{original-logical-bcdc3}.



\section{Experiments and Evaluation}\label{experiment}
To evaluate the data communication, fault tolerance, and node-disjoint paths performance of BCDC, we build a real BCDC network platform $A_3$. The platform $A_3$ contains 12 servers. The server is intended to use a normal PC (Operating System: CentOS Linux 7; Memory: 1.8G GiB; Hard Disk: 500G). One Intel Gigabit PCI-E network adapter (Network Card) is deployed for each server, and the switch uses TP-LINK 8-port Gigabit switch (TL-SG1008PE). Each of these servers uses only two ports on the NIC. The network cable connecting the server and the switch is a twisted pair. In addition, In order to better evaluate the performance of BCDC, we also build two other network platforms DCell$_{1,3}$ and Fat-Tree with 12 servers for comparison experiments. DCell$_{1,3}$ and Fat-Tree have the same experimental configuration as $A_3$. We use modules such as \emph{socket} and \emph{multithreading} in the Python language to implement the corresponding communication algorithms. The important functions and corresponding descriptions in the programs are as follows:
\begin{itemize}
  \item  \emph{totalTransmissionTime()}: Using the \emph{datetime} module to calculate the total program running time.
  \item  \emph{averageCpuUsageRate()}: Using the \emph{psutil} module to calculate the average CPU usage rate.
  \item  \emph{bcdcRouting()}: Building a one-to-one, fault-free transmission path between two nodes in BCDC.
  \item  \emph{bcdcMulticast()}: Using multithreading to construct multiple one-to-one, fault-free paths in BCDC.
  \item  \emph{bcdcDisjointPaths()}: Using multithreading to construct multiple disjoint paths between two nodes in BCDC.
\end{itemize}
In order to improve the accuracy of experimental data, all experimental data are the average value of 20 runs.

\subsection{Communication performance of $A_3$, DCell$_{1,3}$, and Fat-Tree}\label{AA}
We conduct one-to-one, one-to-two, one-to-four, one-to-eight, one-to-eleven(broadcast), and eleven to eleven(all-to-all) experiments of $A_3$, DCell$_{1,3}$, and Fat-Tree, respectively. We intend to send 5G data from one source server to all other destination servers and measure the total transmission time and the average CPU usage rate of the server to evaluate the communication performance of these three data center networks. For the specific communication algorithms in BCDC, we refer to the algorithms given in \cite{wang2018bcdc}. For DCell, our communication algorithms are based on the literature \cite{GuoC2008}. The corresponding communication algorithms for Fat-Tree come from \cite{AlFaresM2008}. In the experiments, we deploy the transmission program on one of the 12 servers in each data center network, and deploy the receiver programs on the other servers, and collect the experimental results after the data communication. The experimental results are shown in Fig. \ref{broadcast}.


\begin{figure}[!h]
\centering
\subfigure[The total transmission time]{
\label{Fig.sub.1}
\includegraphics[scale=0.45]{broadcastTime.eps}}
\subfigure[The  average CPU usage rate]{
\label{Fig.sub.2}
\includegraphics[scale=0.45]{broadcastCpu.eps}}
\caption{The total transmission time and average CPU usage rate of $A_3$, DCell$_{1,3}$, and Fat-Tree in data communication}
\label{broadcast}
\end{figure}

As can be seen from Fig. \ref{Fig.sub.1}, from the perspective of total transmission time, $A_3$ outperforms DCell$_{1,3}$ and Fat-Tree in data communication performance. Especially for one-on-eight,  one-on-eleven, and all to all, the total transmission time of DCell$_{1,3}$ and Fat-Tree is twice or more than the total transmission time of $A_3$. On the other hand, we can see from Fig. \ref{Fig.sub.2} that the average CPU usage rate of $A_3$, DCell$_{1,3}$, and Fat-Tree is almost the same. For example, in the case of all-to-all communication with the highest CPU usage rate, the average CPU usage rates of $A_3$, DCell$_{1,3}$, and Fat-Tree are $89\%$, $86\%$, and $92\%$, respectively. Therefore, it can still be considered that $A_3$ has a better routing performance. In summary, $A_3$ is superior to DCell$_{1,3}$ and Fat-tree in data communication performance.

\subsection{Fault Tolerance of $A_3$, DCell$_{1,3}$, and Fat-Tree}
In this section, we will experiment with fault-tolerant routing for $A_3$, DCell$_{1,3}$, and Fat-Tree, including server fault tolerance, switch fault tolerance, link fault tolerance, and hybrid fault tolerance.
In the fault tolerant experiment of $A_3$ and DCell$_{1,3}$, the number of faulty servers, faulty switches, faulty links  and the number of faulty servers, faulty switches, links in hybrid fault tolerance are 3, 1, 1, (1, 1, 1) and 2, 1, 1, (1, 1, 1). Because the scale of the Fat-Tree we build is small, we do not distinguish the switches in the core layer, aggregation layer and edge layer in the actual fat tree structure of 12 servers, that is, the switches in the core layer, aggregation layer, and edge layer are TL-SG1008PEs. In addition, because the servers in the Fat-Tree are directly connected to the switches in the edge layer, and the connectivity of the Fat-Tree is 1, we will not carry out fault-tolerant experiments on servers failure like BCDC and DCell. Therefore, In the fault tolerant experiment of Fat-Tree, the number of faulty switches, faulty links  and the number of faulty switches, links in hybrid fault tolerance are 1, 1, (1, 1), respectively. We intend to send 5G data from one source server to another destination server and measure the data transfer rate per second and the average CPU usage rate of the server to evaluate the communication performance of these three data center networks. Our fault-tolerant algorithms in BCDC, DCell, and Fat-Tree experiments refer to \cite{wang2018bcdc}, \cite{GuoC2008}, and \cite{AlFaresM2008}, respectively. In the experiment, we deploy the transmission program on one of the 12 servers in each data center network, and deploy the receiver program on another server, and collect the experimental results after data communication. The experimental results are shown in Figure \ref{faultTolerance}.


\begin{figure}[!h]
\centering
\subfigure[The data transfer rate per second]{
\label{Fig.subft.1}
\includegraphics[scale=0.45]{faultToleranceTransferRate.eps}}
\subfigure[The  average CPU usage rate]{
\label{Fig.subft.2}
\includegraphics[scale=0.45]{faultToleranceCpu.eps}}
\caption{The data transfer rate per second and average CPU usage rate of $A_3$, DCell$_{1,3}$, and Fat-Tree in fault tolerance}
\label{faultTolerance}
\end{figure}


Since the faulty server in Fat-Tree has no effect on the data communication of other fault-free servers, we have not carried out fault-tolerant experiments of fault server in Fat-Tree. Therefore, we use 0 to replace the experimental results of data transfer rate per second and the average CPU usage rate in the fault-tolerant experiments of fault server in Fat-Tree.

As can be seen from Fig. \ref{Fig.subft.1}, $A_3$ has faster data transfer rate for four fault-tolerant types than DCell$_{1,3}$ and Fat-Tree. On the other hand, $A_3$'s performance on average CPU usage rate is comparable to that of DCell$_{1,3}$ and Fat-Tree in Fig. \ref{Fig.subft.2}. But on the whole, the experimental results of these three data center networks are not much different. Even in the most complex hybrid fault tolerance, the data transfer rate per second and the average CPU usage rate of $A_3$, DCell$_{1,3}$, and Fat-Tree are only 13.67m/s, 10.72m/s, 10.05m/s and $35\%, 41\%, 34\%$. This means that $A_3$ is no worse than DCell$_{1,3}$ and Fat-tree in terms of fault tolerance.

\subsection{Node-Disjoint Paths of $A_3$ and DCell$_{1,3}$}
The third experiment will perform node-disjoint paths experiment on $A_3$. The connectivity on $A_3$ is 4. According to Merger's theorem, there are four node-disjoint paths between any two nodes in 3-dimensional $A_3$. We will arbitrarily select two nodes in the $A_3$ in the experiment, and transmit 5G data through the four node-disjoint paths between these two nodes, and record the transmission time and the average CPU usage rate(ACUR). Similarly, we will evaluate the node-disjoint paths performance of $A_3$ through a comparison experiment with DCell$_{1,3}$. The connectivity of DCell$_{1,3}$ is 3, so there are three node-disjoint paths between any two nodes of the DCell. Since the connectivity of Fat-Tree is 1, there is no more than one node-disjoint paths between any two nodes of Fat-Tree, we don't carry out the node-disjoint paths experiment of Fat-Tree. Our node-disjoint paths algorithms in BCDC and DCell experiments refer to \cite{WangDisjointPath} and  \cite{WangX2016}. In the experiment, we deploy the transmission program on one of the 12 servers in each data center network, and deploy the receiver program on another server, and collect the experimental results after data communication. The experimental results are shown in Table \ref{node-disjoint paths}.

\begin{table}[!h]
\centering
\caption{The transmission time and the average CPU usage rate of node-disjoint paths in $A_3$ and DCell$_{1,3}$}
%\begin{threeparttable}
\begin{tabular}{|c|c|c|c|}
\hline

\multicolumn{1}{|c|}{ \multirow{2}*{Structure} }&
\multicolumn{2}{|c|}{node-disjoint paths}\\
\cline{2-3}
&time&ACUR\\
\cline{1-3}
$A_3$&220s&43\%\\
\cline{1-3}
DCell$_{1,3}$&245s&46\%\\
\hline
\end{tabular}
\label{node-disjoint paths}
\end{table}

It can be seen from Table \ref{node-disjoint paths}, compared with 328s for one-to-one transmission of 5G data between two nodes in Fig. \ref{Fig.sub.1}, it only takes 220s for $A_3$ to transmit 5G data through four node-disjoint paths between two nodes, which significantly improves the transmission efficiency and effectively improves the data bandwidth. Besides, the performance of  node-disjoint paths in $A_3$ and DCell$_{1,3}$ is similar in terms of transmission time and average CPU usage rate, which also shows that the data center network BCDC has good performance in terms of node-disjoint paths.

Through the above three experiments, we can know that $A_3$ has good properties in data communication, fault-tolerant routing and node-disjoint paths. Although the scale of three experiments is not large, it can also reflect that BCDC is a new data center network with great potential.

\section{Conclusion}\label{conclusion}

In this paper, we carry out experiments of data communication, fault tolerance, and node-disjoint paths of BCDC, and compare them with other two data center networks, DCell and Fat-Tree. The experimental results show that BCDC is superior to DCell and Fat-Tree in data communication. Moreover, in terms of fault-tolerant routing and node-disjoint paths, the performance of BCDC is no worse than that of DCell and Fat-Tree.  This shows that BCDC is a superior new data center network. Although the scale of the experiment is small, our work in this paper also provides an important basis for the future design and application of new data center networks.

\section*{Acknowledgment}

This work was supported by the National Natural Science Foundation of China (No. 61572337, No. 61972272, and No. 61602333), the National Natural Science Foundation of China (No. U1905211), the Natural Science Foundation of the Jiangsu Higher Education Institutions of China (No. 18KJA520009), and the Priority Academic Program Development of Jiangsu Higher Education Institutions.

%\section*{References}

\begin{thebibliography}{00}
%1
\bibitem{Chang2000Edge}
C. P. Chang, T. Y. Sung, L. H. Hsu, ``Edge congestion and topological properties of crossed cubes,'' IEEE Transactions on Parallel and Distributed Systems, vol.11, no.1, pp.64-80, 2000.

%2
\bibitem{GuoJ2019}
J. Guo, D. Li, M. Lu, ``The g-good-neighbor conditional diagnosability of the crossed cubes under the PMC and MM* model,'' Theoretical Computer Science, vol.755, pp.81-88, 2019.

%3
\bibitem{WangS2018}
S. Wang, X. Ma, ``The g-extra connectivity and diagnosability of crossed cubes,'' Applied Mathematics and Computation, vol.336, pp.60-66, 2018.

%4
\bibitem{DBLPEfe}
K. Efe, ``A variation on the hypercube with lower diameter,'' IEEE Transactions on Computers, vol.40, no.11, pp.1312-1316, 1991.

%5
\bibitem{ChenHC20}
H. C. Chen, T. L. Kung, L. H. Hsu, ``An augmented pancyclicity problem of crossed cubes,'' The Computer Journal, vol.61, no.1, pp.54-62, 2018.

%6
\bibitem{EfeK1992}
K. Efe, ``The crossed cube architecture for parallel computation,'' IEEE Transactions on Parallel and distributed Systems, vol.3, no.5, pp.513-524, 1992.

%7
\bibitem{AlFaresM2008}
M. Al-Fares, A. Loukissas, A. Vahdat, ``A scalable, commodity data center network architecture,'' ACM SIGCOMM Computer Communication Review, vol.38, no.4, pp.63-74, 2008.

%8
\bibitem{GuoC2009}
C. X. Guo, G. H. Lu, D. Li, H. T. Wu, X. Zhang, Y. F. Shi, C. Tian,
Y. G. Zhang, S. W. Lu, ``BCube: a high performance, server-centric network architecture for modular data centers,'' ACM SIGCOMM Computer Communication Review, vol.39, no.4, pp.63-74, 2009.

%9
\bibitem{GuoC2008}
C. Guo, H. Wu, K. Tan, L. Shi, Y. G. Zhang, S. W. Lu, ``Dcell: a scalable and fault-tolerant network structure for data centers,'' ACM SIGCOMM Computer Communication Review, vol.38, no.4, pp.75-86, 2009.

%10
\bibitem{Zhengk2016}
K. Zheng, L. Wang, B. H. Yang, Y. Sun, S. Uhlig, ``Lazyctrl: A scalable hybrid network control plane design for cloud data centers,'' IEEE Transactions on Parallel and Distributed Systems, vol.28, no.1, pp.115-127, 2016.

%11
\bibitem{BhuyanLN1984}
L. N. Bhuyan, D. P. Agrawal, ``Generalized hypercube and hyperbus structures for a computer network,'' IEEE Transactions on computers, vol.C-33, no.4, pp.323-333, 1984.

%12
\bibitem{XuM2019}
M. Xu, J. Diakonikolas, E. Modiano, S. Subramaniam, ``A hierarchical WDM-based scalable data center network architecture,'' arXiv preprint arXiv:1901.06450, 2019.

%13
\bibitem{NasirianS2019}
S. Nasirian, F. Faghani, ``Crystal: A scalable and fault-tolerant Archimedean-based server-centric cloud data center network architecture,'' Computer Communications, vol.147, pp.159-179, 2019.

%14
\bibitem{XieP2019}
P. Xie, H. Gu, K. Wang, X. Yu, S. Ma, ``Mesh-of-Torus: a new topology for server-centric data center networks,'' The Journal of Supercomputing, vol.75, no.1, pp.255-271, 2019.

%15
\bibitem{LvM2018}
M. Lv, S. Zhou, X. Sun, G. Lian, J. Liu, ``Reliability evaluation of data center network DCell,'' Parallel Processing Letters, vol.28, no.04, 2018.



%16
\bibitem{MisraS2019}
S. Misra, A. Mondal, S. Khajjayam, ``Dynamic big-data broadcast in fat-tree data center networks with mobile IoT devices,'' IEEE Systems Journal, pp.1-8, 2019.

%17
\bibitem{LeisersonCE1985}
C. E. Leiserson, ``Fat-trees: universal networks for hardware-efficient supercomputing,'' IEEE transactions on Computers, vol.100, no.10, pp.892-901, 1985.



%18
\bibitem{wang2018bcdc}
X. Wang, J. X. Fan, C.-K. Lin, J. Y. Zhou, ``BCDC: a high-performance, server-centric data center network,'' Journal of Computer Science and Technology, vol.33, no.2, pp.400-416, 2018.

%19
\bibitem{WangDisjointPath}
X. Wang, J. Fan, B. Cheng, J. Zhou, S. Zhang, ``Node-disjoint paths in BCDC networks,'' Theoretical Computer Science, 2019, unpublished.

%20
\bibitem{WangX2016}
X. Wang, J. Fan, C.-K Lin, X. Jia, ``Vertex-disjoint paths in DCell networks,'' Journal of Parallel and Distributed Computing, vol.96, pp.38-44, 2016.

\end{thebibliography}
\vspace{12pt}
\color{red}

\end{document}
